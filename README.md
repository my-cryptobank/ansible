sudo pacman -S ansible-core
ansible-galaxy collection install ansible.posix

ansible-playbook ./kubernetes-setup/master-playbook.yml
ansible-playbook ./kubernetes-setup/node-playbook.yml
ansible-playbook ./kubernetes-setup/kube_longhorn.yml   